import express from "express";
import {
	list,
	create,
	find,
	destroy,
	update,
} from "../controller/kategoriController.js";

const router = express.Router();

router.get("/list", list);
router.post("/create", create);
router.get("/find/:id", find);
router.put("/update/:id", update);
router.delete("/delete/:id", destroy);

export default router;