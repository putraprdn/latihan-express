1. Masuk ke terminal MongoShell
    - mongo

2. Membuat 1 database dengan nama 'my_db'
    - use my_db

3. Membuat 3 collections ('user', 'product', 'order')
    - db.createCollection('user')
    - db.createCollection('product')
    - db.createCollection('order')