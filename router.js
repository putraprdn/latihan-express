import express from "express";
import { checkHealth } from "./controller/checkHealthController.js";
import { bio } from "./controller/biodataController.js";
import { checkAuthKey } from "./middleware/checkKey.js";
// import { get as getProduct ,create as createProduct,
//     update as updateProduct ,destroy as deleteProduct,
//     find as findProduct } from "./controller/productController.js";
import productApi from "./api/productApi.js";
import bookApi from "./api/bookApi.js";
import kategoriApi from "./api/kategoriApi.js";

const router = express.Router();

router.get("/check-health", checkHealth);
router.get("/biodata", bio);
router.use("/product", productApi);
router.use("/book", bookApi);
router.use("/kategori", kategoriApi);

// router.get('/product/list', getProduct)
// router.post('/product/create', createProduct)
// router.put('/product/update', updateProduct)
// router.delete('/product/delete', deleteProduct)
// router.post('/product/find', findProduct)

export default router;
