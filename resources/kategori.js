class Kategori {
	static kategoris = [];

	constructor(params) {
		this.id = params.id;
		this.kategori = params.kategori;
		this.deskripsi = params.deskripsi;
	}

	static list() {
		return this.kategoris;
	}

	static create(params) {
		const kategori = new this(params);

		this.kategoris.push(kategori);

		return kategori;
	}

	static find(id) {
		const kategori = this.kategoris.find((i) => i.id === Number(id));
		if (!kategori) return null;

		return kategori;
	}

	update(params) {
		const idx = this.constructor.kategoris.findIndex(
			(i) => i.id === this.id
		);

		params.title && (this.title = params.title);
		params.kategori && (this.kategori = params.kategori);
		params.deskripsi && (this.deskripsi = params.deskripsi);

		this.constructor.kategoris[idx] = this;

		return this;
	}

	delete() {
		this.constructor.kategoris = this.constructor.kategoris.filter(
			(i) => i.id !== this.id
		);
	}
}

export default Kategori;
