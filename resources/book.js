const books = [];

class Book {
	constructor(params) {
		this.id = params.id;
		this.title = params.title;
		this.coverImage = params.coverImage;
		this.synopsis = params.synopsis;
		this.publisher = params.publisher;
		this.author = params.author;
		this.price = params.price;
	}

	static list() {
		return books;
	}

	static create(params) {
		const book = new this(params);

		books.push(book);

		return book;
	}

	static find(id) {
		const book = books.find((i) => i.id === Number(id));
		if (!book) return null;

		return book;
	}
}

export default Book;
