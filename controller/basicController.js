export const mainRoute = (req, res) => {
	return res.send("Hello World");
};

export const withParam = (req, res) => {
	return res.send(`Hello the query param is ${req.query.nama}`);
};

export const withId = (req, res) => {
	return res.send(`hello the param is ${req.params.id}`);
};

export const withBody = (req, res) => {
	return res.send(`the body is ${req.body.nama}`);
};
