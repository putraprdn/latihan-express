import Book from '../resources/book.js'

export const create = (req, res) => {
    const book = Book.create(req.body)

    return res.status(201).json(book)
}

export const list = (req, res) => {
    const books = Book.list();

    return res.status(200).json(books)
}

export const find = (req, res) => {
    const book = Book.find(req.params.id);

    return res.status(200).json(book)
}
