import Kategori from "../resources/kategori.js";

export const create = (req, res) => {
	const kategori = Kategori.create(req.body);

	return res.status(201).json(kategori);
};

export const list = (req, res) => {
	const kategoris = Kategori.list();

	return res.status(200).json(kategoris);
};

export const find = (req, res) => {
	const kategori = Kategori.find(req.params.id);

	return res.status(200).json(kategori);
};

export const update = (req, res) => {
	const kategori = Kategori.find(req.params.id);
	if (!kategori)
		return res.status(404).json({ error: "Kategori tidak ditemukan!" });

	kategori.update(req.body);

	res.status(200).json(kategori);
};

export const destroy = (req, res) => {
	const kategori = Kategori.find(req.params.id);

	if (!kategori)
		return res.status(404).json({ error: "Kategori tidak ditemukan!" });

	kategori.delete();

	res.status(200).end(`Data dengan id: ${req.params.id} telah terhapus`);
};
